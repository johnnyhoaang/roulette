import java.util.Random;
public class RouletteWheel{

    private Random randGen;
    private int number;

    public RouletteWheel() {
        this.randGen = new Random();
        this.number = 0;
    }

    public void spin() { 
        this.number = randGen.nextInt(37);
    }

    public int getValue(){
        return this.number;
    }
    public boolean isLow(){
        if (this.number>=1 && this.number <=18){
            return true;
        }
        return false;
    }

    public boolean isHigh(){
        if (this.number>=19 && this.number <=36){
            return true;
        }
        return false;
    }
    public boolean isEven(){
        if(this.number%2 == 0 && this.number!=0){
            return true;
        }
        return false;
    }
    public boolean isOdd(){
        if(this.number%2 == 1){
            return true;
        }
        return false;
    }
    public boolean isRed(){
        if (this.number>=1 && this.number <= 10
        && this.number>=19 && this.number<=28){
            if (this.number % 2 == 1){
                return true;
            }
        }
        return false;
    }

    public boolean isBlack(){
        if (this.number>=11 && this.number <= 18
        && this.number>=29 && this.number<=36){
            if (this.number % 2 == 1){
                return true;
            }
        }
        return false; 
    }
}
