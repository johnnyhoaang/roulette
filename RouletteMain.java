import java.util.Scanner;
public class RouletteMain {
    public static void main(String[] args){
        RouletteWheel board = new RouletteWheel();

        Scanner reader = new Scanner(System.in);

        //Initianilize values
        int bet = 0;
        int guess = 0;
        String confirmation = " ";
        int totalAmount = 0;
        int endLoop = 0;

        //Creating loop that ends when user inputs -1
        while(endLoop!=-1){

            System.out.println("Insert your bet amount:");
            bet = reader.nextInt();
            System.out.println("Do you want to bet?");
            confirmation = reader.next();

            if (confirmation.equals("Yes")){
                System.out.println("What is your guess?");
            
                board.spin();
                //Test line to guess number
                System.out.println("Testing:" +board.getValue());
                guess= reader.nextInt();
                //Comparing guess to number
                if (guess==board.getValue()){
                    totalAmount += bet*(guess-1);
                }
                else {
                    totalAmount-= bet;
                }
            }
            System.out.println(totalAmount);
            System.out.println("Do you want to continue? If Yes, type any number. If not, type -1");
            endLoop = reader.nextInt();
        }
            System.out.println("Your total amount is:" + totalAmount);
    }
}
